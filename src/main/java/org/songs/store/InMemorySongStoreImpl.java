package org.songs.store;

import java.util.*;

public class InMemorySongStoreImpl implements InMemorySongStore {
    private int capacity;
    Map<String, LinkedList<Song>> userSongMap;

    public InMemorySongStoreImpl(int capacity) {
        this.capacity = capacity;
        this.userSongMap = new HashMap<>();
    }

    public void addRecentlyPlayedSong(String user, Song song){
      //if user doesn't exist
       if(!userSongMap.containsKey(user)){
           userSongMap.put(user, new LinkedList<>());
       }

       //get the list of songs for the given user
        LinkedList<Song> songLinkedList = userSongMap.get(user);

       if(songLinkedList.contains(song)){
           songLinkedList.remove(song);
       }

        songLinkedList.addLast(song);

       if(songLinkedList.size()>capacity){
           songLinkedList.removeFirst();
       }

    }

    public List<Song> getRecentlyPlayedSong(String user){
       return userSongMap.getOrDefault(user,new LinkedList<>());
    }

}
