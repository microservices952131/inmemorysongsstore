package org.songs.store;

import java.util.List;

public interface InMemorySongStore {
    void addRecentlyPlayedSong(String user, Song song);
    List<Song> getRecentlyPlayedSong(String user);
}
