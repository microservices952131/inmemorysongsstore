package org.songs.store;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class InMemoryStoreTest {
    private InMemorySongStoreImpl inMemorySongStore;
    private final int CAPACITY = 3;

    @BeforeMethod
    public void setUp() {
        inMemorySongStore = new InMemorySongStoreImpl(CAPACITY);
    }

    @Test
    public void getRecentlyPlayedSongCapacityTest() {
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S1"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S2"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S3"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S4"));

        int actualCapacity = inMemorySongStore.getRecentlyPlayedSong("U1").size();

        Assert.assertEquals(CAPACITY, actualCapacity, "Expected capacity :" + CAPACITY + " but got " + actualCapacity);
    }

    @Test
    public void getRecentlyPlayedSongForOneUserTest() {
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S1"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S2"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S3"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S2"));

        List<Song> userOneActualSongs = inMemorySongStore.getRecentlyPlayedSong("U1");
        List<Song> expected = List.of(new Song("S1"), new Song("S3"), new Song("S2"));

        Assert.assertEquals(expected, userOneActualSongs, "Expected :" + expected + " but got " + userOneActualSongs);
    }

    @Test
    public void getRecentlyPlayedSongMultipleUsersTest() {
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S1"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S2"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S3"));
        inMemorySongStore.addRecentlyPlayedSong("U1", new Song("S4"));

        inMemorySongStore.addRecentlyPlayedSong("U2", new Song("S2"));
        inMemorySongStore.addRecentlyPlayedSong("U2", new Song("S3"));
        inMemorySongStore.addRecentlyPlayedSong("U2", new Song("S4"));
        inMemorySongStore.addRecentlyPlayedSong("U2", new Song("S5"));

        List<Song> userOneActualSongs = inMemorySongStore.getRecentlyPlayedSong("U1");
        List<Song> userTwoActualSongs = inMemorySongStore.getRecentlyPlayedSong("U2");
        List<Song> userOneExpectedSongs = List.of(new Song("S2"), new Song("S3"), new Song("S4"));
        List<Song> userTwoExpectedSongs = List.of(new Song("S3"), new Song("S4"), new Song("S5"));

        Assert.assertEquals(userOneExpectedSongs, userOneActualSongs, "Expected :" + userOneExpectedSongs + " but got " + userOneActualSongs);
        Assert.assertEquals(userTwoExpectedSongs, userTwoActualSongs, "Expected :" + userTwoExpectedSongs + " but got " + userTwoActualSongs);
    }


}
